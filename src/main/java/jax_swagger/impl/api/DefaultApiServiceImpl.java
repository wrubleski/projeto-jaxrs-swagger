package jax_swagger.impl.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import jax_swagger.api.DefaultApi;
import jax_swagger.models.NewPetDTO;
import jax_swagger.models.PetDTO;

@Path("/")
public class DefaultApiServiceImpl implements DefaultApi {

	static Map<Long, PetDTO> petMap = new HashMap<>();

	public Response addPet(NewPetDTO pet) {

		long id = (long) (petMap.keySet().size() + 1);
		PetDTO newPet = new PetDTO();
		newPet.setName(pet.getName());
		newPet.setTag(pet.getTag());
		newPet.setId(id);
		petMap.put(id, newPet);

		return Response.ok().build();
	}

	public PetDTO deletePet(Long id) {
		PetDTO deletedPet = petMap.get(id);
		petMap.remove(id);
		return deletedPet;
	}

	public Response findPetById(Long id) {
		PetDTO pet = petMap.get(id);
		return Response.ok().entity(pet).build();
	}

	public List<PetDTO> findPets(List<String> tags, Integer limit) {
		List<PetDTO> pets = new ArrayList<>(petMap.values());
		return pets;
	}
}