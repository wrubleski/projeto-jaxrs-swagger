package jax_swagger.impl.api;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class Servidor {

	public static void main(String[] args) {
		try {
			URI uri = UriBuilder.fromUri("http://localhost").port(8080).build();
			ResourceConfig config = new ResourceConfig();
			config.packages("jax_swagger.api");
			config.packages("jax_swagger.impl.api");
			config.packages("jax_swagger.models");

			GrizzlyHttpServerFactory.createHttpServer(uri, config);
			System.out.println("Listening on port 8080");

		} catch (Exception e) {
			System.out.println("Erro na execucao: " + e.getMessage());
		}
	}
}
