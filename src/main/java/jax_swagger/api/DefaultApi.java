package jax_swagger.api;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import jax_swagger.models.ErrorModelDTO;
import jax_swagger.models.NewPetDTO;
import jax_swagger.models.PetDTO;

/**
 * Swagger Petstore
 *
 * <p>
 * A sample API that uses a petstore as an example to demonstrate features in
 * the swagger-2.0 specification
 *
 */

@Api(value = "/", description = "")
public interface DefaultApi {

	@POST
	@Path("/pets")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	@ApiOperation(value = "", tags = {})
	@ApiResponses(value = { @ApiResponse(code = 200, message = "pet response", response = PetDTO.class),
			@ApiResponse(code = 200, message = "unexpected error", response = ErrorModelDTO.class) })
	public Response addPet(@Valid NewPetDTO pet);

	@DELETE
	@Path("/pets/{id}")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	@ApiOperation(value = "", tags = {})
	@ApiResponses(value = { @ApiResponse(code = 204, message = "pet deleted"),
			@ApiResponse(code = 200, message = "unexpected error", response = ErrorModelDTO.class) })
	public PetDTO deletePet(@PathParam("id") Long id);

	@GET
	@Path("/pets/{id}")
	@Consumes({ "application/json" })
	@Produces({ "application/json", "application/xml", "text/xml", "text/html" })
	@ApiOperation(value = "", tags = {})
	@ApiResponses(value = { @ApiResponse(code = 200, message = "pet response", response = PetDTO.class),
			@ApiResponse(code = 200, message = "unexpected error", response = ErrorModelDTO.class) })
	public Response findPetById(@PathParam("id") Long id);

	@GET
	@Path("/pets")
	@Consumes({ "application/json" })
	@Produces({ "application/json", "application/xml", "text/xml", "text/html" })
	@ApiOperation(value = "", tags = {})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "pet response", response = PetDTO.class, responseContainer = "List"),
			@ApiResponse(code = 200, message = "unexpected error", response = ErrorModelDTO.class) })
	public List<PetDTO> findPets(@QueryParam("tags") List<String> tags, @QueryParam("limit") Integer limit);
}
